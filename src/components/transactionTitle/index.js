import React from 'react';
import Wrapper from './style';

const index = (props) => {
  const { title } = props;

  return <Wrapper>{title}</Wrapper>;
};

export default index;
