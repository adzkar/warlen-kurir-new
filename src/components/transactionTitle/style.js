import styled from 'styled-components';

const Wrapper = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 15px;
  line-height: 18px;
  letter-spacing: -0.04em;
  margin-bottom: 10px;
  color: #e0003c;
`;

export default Wrapper;
