import React from 'react';
import Wrapper from './style';

const index = (props) => {
  const { title, children } = props;

  return (
    <Wrapper>
      <div className="component_location_title">{title}</div>
      <div className="component_location_card">{children}</div>
    </Wrapper>
  );
};

export default index;
