import styled from 'styled-components';
// import color from '../../configs/color';

const Wrapper = styled.div`
  margin-bottom: 7px;

  .component_location_title {
    font-style: normal;
    font-weight: bold;
    font-size: 15px;
    line-height: 18px;
    letter-spacing: -0.04em;
    color: #343434;
    margin-bottom: 13px;
  }

  .component_location_card {
    border: 1px solid rgba(196, 196, 196, 0.3);
    box-sizing: border-box;
    border-radius: 10px;
    padding: 12px;
  }
`;

export default Wrapper;
