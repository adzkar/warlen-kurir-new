import React from 'react';
import Wrapper from './style';

const Index = (props) => {
  const { children } = props;
  return (
    <Wrapper>
      <div className="component_content_form_wrapper">{children}</div>
    </Wrapper>
  );
};

export default Index;
