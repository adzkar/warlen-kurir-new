import styled from 'styled-components';
import BREAKPOINTS from '../../configs/breakpoints';

const Wrapper = styled.div`
  .component_content_form_wrapper {
    margin: auto;
    max-width: 450px;
    background: white;
    padding: 40px 45px 47px 45px;
    border-radius: 16px;
    border: 1px solid var(--black200);
    margin-top: 100px;

    @media screen and (max-width: ${BREAKPOINTS.PHONE}) {
      padding: 40px 30px 64px 30px;
    }

    h1 {
      color: var(--green600);
      text-align: center;
      margin-bottom: 15px;
    }

    hr {
      width: 83%;
      border: 0.5px solid var(--black200);
      margin-bottom: 27px;
      border-radius: 1px;
    }
  }

  .container_form_button.ant-form-item {
    margin-bottom: 12px;
  }
`;

export default Wrapper;
