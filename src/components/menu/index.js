import React from 'react';
import { Menu } from 'antd';
import { useHistory } from 'react-router-dom';
import Wrapper from './style';
import { AuthService } from '../../services';
import URL from '../../configs/baseUrl';

const Index = () => {
  const history = useHistory();

  const onTransaction = () => {
    history.push(URL.TRANSACTION);
  };

  const onLogout = () => {
    AuthService.logout();
  };

  return (
    <Wrapper>
      <Menu mode="vertical">
        <Menu.Item key="1">Pemberitahuan</Menu.Item>
        <Menu.Item key="2" onClick={onTransaction}>
          Status Pesanan
        </Menu.Item>
        <Menu.Item key="3" onClick={onLogout}>
          Logout
        </Menu.Item>
      </Menu>
    </Wrapper>
  );
};

export default Index;
