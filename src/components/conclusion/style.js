import styled from 'styled-components';
import color from '../../configs/color';

const Wrapper = styled.div`
  margin-bottom: 70px;

  .component_location_title {
    font-style: normal;
    font-weight: bold;
    font-size: 15px;
    line-height: 18px;
    letter-spacing: -0.04em;
    color: #343434;
    margin-bottom: 13px;
  }

  .component_location_card {
    border: 1px solid rgba(196, 196, 196, 0.3);
    box-sizing: border-box;
    border-radius: 10px;
    padding: 12px;
    display: flex;
  }

  .component_location_icon {
    margin-right: 10px;
    font-size: 16px;
    color: ${color.red};
  }

  table {
    width: 100%;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 15px;
    color: #565656;
  }

  tbody > tr > td:nth-child(2) {
    text-align: right;
  }

  tbody > tr > td {
    padding-bottom: 7px;
  }

  .component_total {
    font-weight: bold;
  }
`;

export default Wrapper;
