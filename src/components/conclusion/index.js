import React from 'react';
import Wrapper from './style';
import { ToPrice } from '../../utils/converter';

const index = (props) => {
  const { total = 0 } = props;

  return (
    <Wrapper>
      <div className="component_location_title">Detail Pembayaran</div>
      <table>
        <tbody>
          <tr>
            <td>Total Harga Pesanan</td>
            <td>{ToPrice(Math.round(total))}</td>
          </tr>
          <tr>
            <td>Biaya Ongkir</td>
            <td>{ToPrice(3000)}</td>
          </tr>
          <tr className="component_total">
            <td>Total Pembayaran</td>
            <td>{ToPrice(Math.round(total) + 3000)}</td>
          </tr>
        </tbody>
      </table>
    </Wrapper>
  );
};

export default index;
