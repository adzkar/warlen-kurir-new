import styled from 'styled-components';
import color from '../../configs/color';
// import BREAKPOINTS from '../../configs/breakpoints';

const Wrapper = styled.div`
  padding-top: 27px;
  padding-bottom: 27px;
  height: 78px;
  background: #f1f1f1;
  margin-bottom: 29px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;

  .component_header_page_back_button {
    font-size: 20px;
    color: ${color.red};
    font-weight: bold;
    cursor: pointer;
    position: absolute;
    left: 32px;
  }

  .component_header_page_title {
    color: #343434;
    font-weight: 300;
    font-size: 20px;
    line-height: 24px;
    letter-spacing: -0.04em;
  }
`;

export default Wrapper;

export const StickyWrapper = styled.div`
  position: sticky;
  position: -webkit-sticky;
  top: 0;
`;
