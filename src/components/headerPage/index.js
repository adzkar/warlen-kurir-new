import React from 'react';
import { Affix } from 'antd';
import { useHistory } from 'react-router-dom';
import { ArrowLeftOutlined } from '@ant-design/icons';
import Wrapper from './style';

const Index = (props) => {
  const { className, title, backButton } = props;
  const history = useHistory();

  return (
    <Affix>
      <Wrapper className={className ?? ''}>
        {backButton && (
          <div
            className="component_header_page_back_button"
            aria-hidden="true"
            onClick={() => {
              history.goBack();
            }}
          >
            <ArrowLeftOutlined />
          </div>
        )}
        <div className="component_header_page_title">{title}</div>
      </Wrapper>
    </Affix>
  );
};

export default Index;
