import React from 'react';
import { EnvironmentOutlined } from '@ant-design/icons';
import Wrapper from './style';

const index = () => {
  return (
    <Wrapper>
      <div className="component_location_title">Alamat Pengiriman</div>
      <div className="component_location_card">
        <div className="component_location_icon">
          <EnvironmentOutlined />
        </div>
        <div>Jln. Sukabirus No.23 Bojongsoang . . .</div>
      </div>
    </Wrapper>
  );
};

export default index;
