import React from 'react';
import Wrapper from './style';
import MenuItem from './menuitem';

const index = (props) => {
  const { className, title, items = [], onClickItem } = props;

  return (
    <Wrapper className={className ?? ''}>
      <h3 className="font-subtitle1_bold">{title}</h3>
      {items.map((item) => {
        return <MenuItem item={item} key={item.id} onClickItem={onClickItem} />;
      })}
    </Wrapper>
  );
};

export default index;
