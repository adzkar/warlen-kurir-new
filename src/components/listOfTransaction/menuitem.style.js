import styled from 'styled-components';
// import color from '../../configs/color';
// import BREAKPOINTS from '../../configs/breakpoints';

const Wrapper = styled.div`
  margin-bottom: 14px;
  display: flex;
  align-items: flex-start;
  cursor: pointer;

  .component_transaction_image_wrapper {
    width: 60px;
    height: 60px;
    border-radius: 10px;
    background-color: #ededed;
    margin-right: 17px;

    img {
      text-indent: 100%;
      white-space: nowrap;
      overflow: hidden;
      color: transparent;
    }
  }

  .component_transaction_item_desc {
    font-style: normal;
    font-weight: 300;
    font-size: 12px;
    line-height: 12px;
    letter-spacing: -0.04em;
    color: #343434;
    margin-bottom: 6px;
  }

  .component_transaction_item_status {
    font-style: normal;
    font-weight: bold;
    font-size: 12px;
    line-height: 12px;
    letter-spacing: -0.04em;
    color: #e0003c;
    text-transform: capitalize;
  }
`;

export default Wrapper;
