import React from 'react';
import { Divider } from 'antd';
import Wrapper from './menuitem.style';
import { ToPrice } from '../../utils/converter';

const Index = (props) => {
  const { item, onClickItem } = props;

  return (
    <>
      <Wrapper
        aria-hidden="true"
        onClick={() => {
          onClickItem(item);
        }}
      >
        <div className="component_transaction_image_wrapper">
          <img src={undefined} alt={item.name} />
        </div>
        <div>
          <div className="component_transaction_item_title font-subtitle1_bold">
            {`Pesanan #${item.id}`}
          </div>
          <div className="component_transaction_item_desc">
            <strong>Total</strong>
            {` ${ToPrice(item.total)}`}
          </div>
          <div className="component_transaction_item_status">{item.status}</div>
        </div>
      </Wrapper>
      <Divider style={{ margin: 0 }} />
    </>
  );
};

export default Index;
