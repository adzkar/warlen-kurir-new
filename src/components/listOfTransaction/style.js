import styled from 'styled-components';
// import BREAKPOINTS from '../../configs/breakpoints';
// import color from '../../configs/color';

const Wrapper = styled.div`
  & > h1 {
    margin-bottom: 16px;
  }
  margin-bottom: 20px;
`;

export default Wrapper;
