import React from 'react';
import Wrapper from './style';

const Index = (props) => {
  const { children, className, width, withPadding } = props;
  return (
    <Wrapper
      className={className ?? ''}
      width={width}
      withPadding={withPadding}
    >
      <div className="component_content_wrapper">{children}</div>
    </Wrapper>
  );
};

export default Index;
