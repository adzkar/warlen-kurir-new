import styled, { css } from 'styled-components';
import BREAKPOINTS from '../../configs/breakpoints';

const Wrapper = styled.div`
  max-width: ${BREAKPOINTS.PHONE};
  width: 100%;
  margin: auto;
  background-color: white;
  min-height: 100vh;

  .component_content_wrapper {
    max-width: ${({ width }) => {
      return width ? `${width}px` : '1200px';
    }};

    @media (max-width: 1280px) and (min-width: 576px) {
      margin-left: ${(props) => {
        return props.width ? 'auto' : '40px';
      }};
      margin-right: ${(props) => {
        return props.width ? 'auto' : '40px';
      }};
    }

    ${(props) => {
      return (
        props.width &&
        css`
          @media (max-width: calc(${props.width}px + 80px)) and (min-width: 576px) {
            margin-left: 40px;
            margin-right: 40px;
          }
        `
      );
    }}
    position: relative;

    ${(props) => {
      return (
        props.withPadding &&
        css`
          padding-left: 32px;
          padding-right: 32px;
        `
      );
    }}

    @media screen and (max-width: ${BREAKPOINTS.PHONE}) {
      width: 100%;
    }
    margin: auto;
    position: relative;
  }
`;

export default Wrapper;
