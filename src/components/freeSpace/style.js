import styled from 'styled-components';

const FreeSpaceWrapper = styled.div`
  height: ${(props) => {
    return props.height;
  }};
`;
export default FreeSpaceWrapper;
