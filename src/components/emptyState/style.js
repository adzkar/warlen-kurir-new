import styled from 'styled-components';
import { Button as ButtonAntd } from 'antd';
import BREAKPOINTS from '../../configs/breakpoints';
import color from '../../configs/color';

const Wrapper = styled.div`
  text-align: center;
  margin-top: 38px;
  margin-bottom: 112px;

  img {
    max-width: 400px;
    width: 57%;

    @media screen and (max-width: ${BREAKPOINTS.PHONE}) {
      max-width: 250px;
      width: 70%;
    }
  }

  @media screen and (max-width: ${BREAKPOINTS.PHONE}) {
    h3.font-title3_medium {
      font-size: 16px;
    }
  }
`;

export const Button = styled(ButtonAntd)`
  margin-top: 32px;
  background-color: ${color.orange600};
  border-color: ${color.orange600};
  @media screen and (max-width: ${BREAKPOINTS.PHONE}) {
    margin-top: 28px;
  }
  :hover {
    background-color: ${color.orange700};
    border-color: ${color.orange700};
  }
  :focus {
    background-color: ${color.orange700};
    border-color: ${color.orange700};
  }
`;

export default Wrapper;
