import React from 'react';
import Wrapper, { Button } from './style';
import { EmptyFolder } from '../../assets';

const index = (props) => {
  const { text, ctaButton, onClickButton, className } = props;
  return (
    <Wrapper className={className ?? ''}>
      <img src={EmptyFolder} alt="empty" />
      <h3 className="font-title3_medium">{text ?? 'No Records to Display'}</h3>
      {ctaButton && (
        <Button type="primary" onClick={onClickButton}>
          {ctaButton}
        </Button>
      )}
    </Wrapper>
  );
};

export default index;
