import styled from 'styled-components';
// import color from '../../configs/color';
// import BREAKPOINTS from '../../configs/breakpoints';

const Wrapper = styled.div`
  background-color: #69ead3;
  border-radius: 10px;
  height: 178px;
  margin-bottom: 27px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Wrapper;
