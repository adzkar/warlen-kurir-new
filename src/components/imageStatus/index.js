import React from 'react';
import Wrapper from './style';
import { Ongoing } from '../../assets';

const Index = (props) => {
  const { className } = props;

  return (
    <Wrapper className={className ?? ''}>
      <img alt="ongoing" src={Ongoing} />
    </Wrapper>
  );
};

export default Index;
