import React, { lazy } from 'react';
import { Helmet } from 'react-helmet-async';

const Transactions = lazy(() => {
  return import('../../containers/transactions');
});

const MainPage = () => {
  return (
    <>
      <Helmet>
        <title>Transaction</title>
      </Helmet>
      <Transactions />
    </>
  );
};

export default MainPage;
