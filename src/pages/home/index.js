import React, { lazy } from 'react';
import { Helmet } from 'react-helmet-async';

const Home = lazy(() => {
  return import('../../containers/home');
});

const MainPage = () => {
  return (
    <>
      <Helmet>
        <title>Home</title>
      </Helmet>
      <Home />
    </>
  );
};

export default MainPage;
