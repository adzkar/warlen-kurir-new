import React, { lazy, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { AuthService } from '../../services';

const Login = lazy(() => {
  return import('../../containers/login');
});

const MainPage = () => {
  useEffect(() => {
    AuthService.isUserAuthenticatedRedirect();
  }, []);

  return (
    <>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <Login />
    </>
  );
};

export default MainPage;
