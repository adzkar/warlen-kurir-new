import React, { lazy } from 'react';
import { Helmet } from 'react-helmet-async';

const DetailTransaction = lazy(() => {
  return import('../../containers/detailTransaction');
});

const MainPage = () => {
  return (
    <>
      <Helmet>
        <title>Transaction</title>
      </Helmet>
      <DetailTransaction />
    </>
  );
};

export default MainPage;
