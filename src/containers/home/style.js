import styled from 'styled-components';
import color from '../../configs/color';

const Wrapper = styled.div`
  .container_home_content_wrapper .component_content_wrapper {
    min-height: 90vh;
  }

  .container_home_wrapper {
    min-height: unset;
  }

  .container_home_wrapper .component_content_wrapper {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  .container_home_top {
    padding-top: 30px;
    svg {
      color: ${color.red};
    }

    .container_home_menu_button {
      cursor: pointer;
      display: inline-block;
    }
  }

  .container_home_image_wrapper {
    width: 100%;
    text-align: center;
    margin-top: 30px;

    img {
      width: 70%;
    }
  }

  .container_home_content {
    margin-top: -20px;
  }

  .container_home_content h1 {
    font-weight: 300;
    font-size: 30px;
    line-height: 37px;
    letter-spacing: -0.04em;
    color: #343434;
  }

  .container_home_content h1:nth-child(2) {
    font-weight: bold;
  }

  .container_home_button {
    display: flex;
    justify-content: space-between;
    align-items: center;

    .container_home_button_left {
      font-style: normal;
      font-size: 1em;
      line-height: 24px;
      letter-spacing: -0.04em;
      color: #ffffff;
      text-align: left;
    }

    h4 {
      color: white;
      font-weight: bold;
      margin-bottom: 0px;
    }

    p {
      margin-bottom: 0px;
    }

    .container_home_icons {
      svg {
        font-size: 20px;
      }
    }
  }
`;

export default Wrapper;
