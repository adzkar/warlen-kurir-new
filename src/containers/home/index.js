import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from 'antd';
import { MenuOutlined, ContainerOutlined } from '@ant-design/icons';
import Wrapper from './style';
import { ContentWrapper, Menu, ClickOut, FreeSpace } from '../../components';
import { Dish } from '../../assets';
import URL from '../../configs/baseUrl';
import { AuthService } from '../../services';

const Index = () => {
  const history = useHistory();
  const { name } = AuthService.getUserData();
  const [isShow, setIsShow] = useState(false);

  return (
    <Wrapper>
      <ContentWrapper className="container_home_content_wrapper">
        <ContentWrapper withPadding className="container_home_wrapper">
          <div className="container_home_top">
            <div
              className="container_home_menu_button"
              aria-hidden="true"
              onClick={() => {
                setIsShow(true);
              }}
            >
              <MenuOutlined />
            </div>
            {isShow && (
              <ClickOut
                onClickOutside={() => {
                  setIsShow(false);
                }}
              >
                <Menu />
              </ClickOut>
            )}
          </div>
          <div className="container_home_content">
            <h1>Halo</h1>
            <h1>{name}</h1>
            <div className="container_home_image_wrapper">
              <img alt="dish" src={Dish} />
            </div>
          </div>
          <FreeSpace height={10} />
        </ContentWrapper>
        <Button
          type="primary"
          block
          className="container_home_button"
          onClick={() => {
            history.push(URL.TRANSACTION);
          }}
        >
          <div className="container_home_button_left">
            <h4>Lihat Pesanan</h4>
          </div>
          <div className="container_home_icons">
            <ContainerOutlined />
          </div>
        </Button>
      </ContentWrapper>
    </Wrapper>
  );
};

export default Index;
