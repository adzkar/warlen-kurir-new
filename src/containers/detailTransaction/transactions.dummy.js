export const transactaions = [
  {
    id: 1,
    name: 'Pesanan #1',
    desc:
      'Nikmatnya perpaduan Indomie telur dengan perpaduan telur ceplok yang nikmat',
    status: 'on going',
  },
  {
    id: 3,
    name: 'Pesanan #3',
    desc:
      'Nikmatnya perpaduan Indomie telur dengan perpaduan telur ceplok yang nikmat',
    status: 'on going',
  },
  {
    id: 1,
    name: 'Pesanan #1',
    desc:
      'Nikmatnya perpaduan Indomie telur dengan perpaduan telur ceplok yang nikmat',
    status: 'finished',
  },
  {
    id: 2,
    name: 'Pesanan #2',
    desc:
      'Nikmatnya perpaduan Indomie telur dengan perpaduan telur ceplok yang nikmat',
    status: 'canceled',
  },
  {
    id: 3,
    name: 'Pesanan #3',
    desc:
      'Nikmatnya perpaduan Indomie telur dengan perpaduan telur ceplok yang nikmat',
    status: 'canceled',
  },
];

export default transactaions;
