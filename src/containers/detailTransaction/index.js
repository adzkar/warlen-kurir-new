import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Button, Skeleton, message } from 'antd';
import Wrapper from './style';
import {
  ContentWrapper,
  FreeSpace,
  HeaderPage,
  // ListOfTransaction,
  ImageStatus,
  TransactionTitle,
  Location,
  DetailItemTransaction,
  Conclusion,
} from '../../components';
import URL from '../../configs/baseUrl';
import { TransactionsService } from '../../services';
// import transactions from './transactions.dummy';

const Index = () => {
  const history = useHistory();
  const { transactionId } = useParams();

  const [isLoading, setIsLoading] = useState(false);
  const [transactionDetail, setTransactionDetail] = useState();

  useEffect(() => {
    setIsLoading(true);
    TransactionsService.getTransactionById(transactionId)
      .then((res) => {
        setTransactionDetail(res);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [transactionId]);

  const onFinishTransaction = () => {
    const data = {
      status: 'ongoing',
    };
    TransactionsService.putTransactionById({
      id: transactionId,
      body: data,
    })
      .then(() => {
        message.success('Berhasil memperbarui status').then(() => {
          history.push(URL.HOME);
        });
      })
      .catch(() => {
        message.error('Tolong Periksa Konesi anda');
      });
  };

  // const onClickItem = (item) => {
  //   history.push(URL.TRANSACTION_ID(item.id));
  // };

  return (
    <Wrapper>
      <ContentWrapper>
        <HeaderPage title="Status Pesanan" backButton />
        <ContentWrapper withPadding>
          {isLoading ? (
            <Skeleton />
          ) : (
            <>
              <ImageStatus />
              <TransactionTitle
                title={`Pesanan #${transactionDetail?.id} : ${transactionDetail?.status}`}
              />
              <Location />
              <DetailItemTransaction title="No Telepon">
                <>082291865050</>
              </DetailItemTransaction>
              {/* <ListOfTransaction
                title="Data Pesanan"
                items={transactions}
                onClickItem={onClickItem}
              /> */}
              <DetailItemTransaction title="Metode Pembayaran">
                <>Cash On Delivery (COD)</>
              </DetailItemTransaction>
              <Conclusion total={transactionDetail?.total} />
              <Button type="primary" block onClick={onFinishTransaction}>
                Pesanan Selesai
              </Button>
            </>
          )}
        </ContentWrapper>
        <FreeSpace height={50} />
        <Button type="ghost" block>
          Laporkan
        </Button>
      </ContentWrapper>
    </Wrapper>
  );
};

export default Index;
