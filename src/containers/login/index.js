import React, { lazy } from 'react';
import Wrapper, { HeaderWrapper } from './style';
import { ContentWrapper, FreeSpace } from '../../components';

const LoginForm = lazy(() => {
  return import('./form');
});

const Header = () => {
  return (
    <HeaderWrapper>
      <h1>Halo Kurir</h1>
      <p>Silahkan masuk terlebih dalu untuk memulai pengantaranmu!</p>
    </HeaderWrapper>
  );
};

const Index = () => {
  return (
    <Wrapper>
      <ContentWrapper className="container_login_wrapper">
        <Header />
        <LoginForm />
      </ContentWrapper>
      <FreeSpace height={150} />
    </Wrapper>
  );
};

export default Index;
