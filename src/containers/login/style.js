import styled from 'styled-components';

const Wrapper = styled.div`
  .container_login_wrapper {
    display: flex;
    align-items: center;
    padding-left: 27px;
    padding-right: 27px;
  }

  h1 {
    font-style: normal;
    font-weight: bold;
    font-size: 36px;
    line-height: 44px;
    color: #484848;
  }

  p {
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 22px;
    max-width: 500px;
  }
`;

export default Wrapper;

export const HeaderWrapper = styled.div``;
