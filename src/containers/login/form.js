import React, { useState } from 'react';
import axios from 'axios';
import { Form, Button, Input, message } from 'antd';
import { useMediaQuery } from 'react-responsive';
import BREAKPOINTS from '../../configs/breakpoints';
import { AuthService } from '../../services';
import REST from '../../configs/rest';
import URL from '../../configs/baseUrl';
import ROLE from '../../configs/roles';
import { setCookie } from '../../utils/cookies';

const Index = () => {
  const [isLoading] = useState(false);

  const isPhone = useMediaQuery({
    query: `(max-width: ${BREAKPOINTS.PHONE})`,
  });

  const onSubmit = (body) => {
    const data = {
      email: body.email.trim(),
      password: body.password.trim(),
    };
    AuthService.postLogin(data)
      .then((res) => {
        const { token } = res.data;
        axios
          .get(`${process.env.REACT_APP_REST_URL}${REST.ME}`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((account) => {
            const role = account.data.roles[0];
            if (role === ROLE.COURIER) {
              setCookie('userData', JSON.stringify(account.data));
              setCookie('token', JSON.stringify(res.data));
              window.location.replace(URL.HOME);
            }
          });
      })
      .catch(() => {
        message.error('Silahkan cek email atau password anda');
      });
  };

  return (
    <Form
      name="basic"
      layout="vertical"
      className="container_form_login"
      onFinish={onSubmit}
      // onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Email"
        name="email"
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Silahkan masukkan username anda!',
          },
          {
            type: 'email',
            message: 'Silahkan cek email anda!',
          },
        ]}
      >
        <Input placeholder="Masukkan username anda" />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Silahkan masukkan password anda!',
          },
        ]}
      >
        <Input.Password placeholder="Masukkan password anda" />
      </Form.Item>

      <Form.Item className="container_form_button">
        <Button
          type="primary"
          block
          htmlType="submit"
          size={isPhone ? 'medium' : 'large'}
          disabled={isLoading}
          loading={isLoading}
        >
          Login
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Index;
