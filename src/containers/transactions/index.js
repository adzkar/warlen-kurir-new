import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Skeleton } from 'antd';
import Wrapper from './style';
import {
  ContentWrapper,
  FreeSpace,
  HeaderPage,
  ListOfTransaction,
} from '../../components';
import { TransactionsService } from '../../services';
import URL from '../../configs/baseUrl';

const Index = () => {
  const history = useHistory();

  const [transactions, setTransactions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    TransactionsService.getTransactions()
      .then((res) => {
        setTransactions(res.data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  const onClickItem = (item) => {
    history.push(URL.TRANSACTION_ID(item.id));
  };

  return (
    <Wrapper>
      <ContentWrapper>
        <HeaderPage title="Status Pesanan" backButton />
        <ContentWrapper withPadding>
          {isLoading ? (
            <Skeleton />
          ) : (
            <ListOfTransaction items={transactions} onClickItem={onClickItem} />
          )}
        </ContentWrapper>
        <FreeSpace height={150} />
      </ContentWrapper>
    </Wrapper>
  );
};

export default Index;
