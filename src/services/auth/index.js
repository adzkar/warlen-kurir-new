import { getCookie, setCookie } from '../../utils/cookies';
import BaseService from '../baseServices';
import REST from '../../configs/rest';
import URL from '../../configs/baseUrl';

function getTokenAuth() {
  if (getCookie('token') !== '' && getCookie('token') !== 'undefined') {
    return JSON.parse(getCookie('token')).value;
  }
  return '';
}

function getUserId() {
  if (getCookie('userData') !== '') {
    return JSON.parse(getCookie('userData'))?.id;
  }
  return '';
}

function getUserRole() {
  if (getCookie('userData') !== '') {
    return JSON.parse(getCookie('userData'))?.role?.name;
  }
  return '';
}

function getUserData() {
  if (getCookie('userData') !== '' && getCookie('userData') !== 'undefined') {
    return JSON.parse(getCookie('userData'));
  }
  return '';
}
// is user is logged in
function isUserAuthenticated() {
  return getTokenAuth() !== '' && getUserData() !== '';
}

function isUserAuthenticatedRedirect() {
  if (isUserAuthenticated()) {
    window.location.replace(URL.HOME);
  }
  return '';
}

function postLogin(body) {
  return BaseService.post(REST.LOGIN, body);
}

function logout() {
  setCookie('userData', '');
  setCookie('token', '');
  window.location.replace(URL.LOGIN);
}

function postRegister(body) {
  return BaseService.post(REST.REGISTER, body);
}

function postForgotPassword(body) {
  return BaseService.post(REST.FORGOT_PASSWORD, body);
}

function postEmailAvailability(body) {
  return BaseService.post(REST.EMAIL_AVAILABILITY, body);
}

function getMe() {
  return BaseService.get(REST.ME);
}

export default {
  isUserAuthenticated,
  getTokenAuth,
  getUserData,
  getUserId,
  postLogin,
  logout,
  postRegister,
  postForgotPassword,
  postEmailAvailability,
  isUserAuthenticatedRedirect,
  getUserRole,
  getMe,
};
