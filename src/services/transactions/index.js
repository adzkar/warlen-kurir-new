import BaseService from '../baseServices';
import REST from '../../configs/rest';

function getTransactions() {
  return BaseService.get(REST.ORDERS);
}

function getTransactionById(id) {
  return BaseService.get(REST.ORDERS_ID(id));
}

function putTransactionById({ id, body }) {
  return BaseService.put(REST.ORDERS_ID(id), body);
}

export default {
  getTransactions,
  getTransactionById,
  putTransactionById,
};
