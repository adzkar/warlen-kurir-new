/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
} from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';
import { Spin } from 'antd';
import 'antd/dist/antd.less';
import './configs/color.less';
import routes from './configs/route';
import { AuthService } from './services';
import AppWrapper from './App.style';
import URL from './configs/baseUrl';
// import ReactGA from 'react-ga';

const PrivateRoute = ({
  component: Component,
  exact,
  accessRoles,
  path,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={() => {
        if (AuthService.isUserAuthenticated()) {
          return <Component />;
        }
        return <Redirect to={URL.LOGIN} />;
      }}
    />
  );
};

const App = () => {
  // ReactGA.initialize(process.env.REACT_APP_GA);
  // ReactGA.pageview(window.location.pathname + window.location.search);
  // const location = useLocation();

  return (
    <HelmetProvider>
      <AppWrapper>
        <Router>
          <React.Suspense fallback={<Spin />}>
            <Switch>
              {routes.map((route) => {
                if (route.isPublic) {
                  return (
                    <Route
                      path={route.path}
                      component={route.component}
                      key={route.path}
                      exact
                    />
                  );
                }
                return (
                  <PrivateRoute
                    path={route.path}
                    component={route.component}
                    key={route.path}
                    accessRoles={route.access}
                  />
                );
              })}
            </Switch>
          </React.Suspense>
        </Router>
      </AppWrapper>
    </HelmetProvider>
  );
};

export default App;
