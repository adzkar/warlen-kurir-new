import styled from 'styled-components';

const AppWrapper = styled.div`
  background-color: var(--bg);
  min-height: 100vh;
  overflow: hidden;
  font-family: OpenSans;

  .ant-btn {
    border-radius: 10px;
  }

  .ant-btn-primary.ant-btn-block {
    padding-top: 14px;
    padding-bottom: 14px;
    height: unset;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
    margin-top: 60px;
  }

  .font-title1 {
    font-size: 36px;
    font-weight: 400;
  }
  .font-title1_medium {
    font-size: 36px;
    font-weight: 600;
  }
  .font-title2 {
    font-size: 24px;
    font-weight: 400;
  }
  .font-title2_medium {
    font-size: 24px;
    font-weight: 600;
  }
  .font-title3 {
    font-size: 18px;
    font-weight: 400;
  }
  .font-title3_medium {
    font-size: 18px;
    font-weight: 600;
  }
  .font-subtitle {
    font-size: 16px;
    font-weight: 400;
  }
  .font-subtitle_medium {
    font-size: 16px;
    font-weight: 600;
  }
  .font-subtitle1_bold {
    font-weight: bold;
    font-size: 16px;
  }
  .font-paragraph {
    font-size: 14px;
    font-weight: 400;
  }
  .font-paragraph_medium {
    font-size: 14px;
    font-weight: 600;
  }
  .font-paragraph_bold {
    font-size: 14px;
    font-weight: 700;
  }
  .font-desc {
    font-size: 14px;
    font-weight: 400;
  }
  .font-desc_medium {
    font-size: 14px;
    font-weight: 600;
  }
  .tetxt-center {
    text-align: center;
  }
`;

export default AppWrapper;
