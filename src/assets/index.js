export { default as HamburgerBar } from './images/hamburger-bar.svg';
export { default as LogoPMT } from './images/logo-pmt.svg';
export { default as EmptyFolder } from './images/empty-folder.svg';
export { default as FileText } from './images/file-text.svg';
export { default as More } from './images/more.svg';
export { default as CardBackground } from './images/card-bg.svg';
export { default as Ongoing } from './images/ongoing.svg';
export { default as Dish } from './images/dish.svg';
