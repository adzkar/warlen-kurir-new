import React from 'react';
import URL from './baseUrl';

const RouteComp = (lazyImport) => {
  return React.lazy(() => {
    return lazyImport;
  });
};

const routes = [
  // public Routes
  {
    path: URL.LOGIN,
    component: RouteComp(import('../pages/login')),
    isPublic: true,
  },
  // private routes
  {
    path: URL.TRANSACTION_ID(':transactionId'),
    component: RouteComp(import('../pages/transaction')),
    isPublic: false,
  },
  {
    path: URL.TRANSACTION,
    component: RouteComp(import('../pages/listOfTransactions')),
    isPublic: false,
  },
  {
    path: URL.HOME,
    component: RouteComp(import('../pages/home')),
    isPublic: false,
  },
];
export default routes;
