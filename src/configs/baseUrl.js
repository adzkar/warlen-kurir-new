export default {
  LOGIN: '/login',
  LOGOUT: '/logout',
  HOME: '/',
  TRANSACTION: '/transaction',
  TRANSACTION_ID: (transactionId) => {
    return `/transaction/${transactionId}`;
  },
};
