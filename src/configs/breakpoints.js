export default {
  DESKTOP: '1200px',
  PHONE: '575px',
  TAB: '768px',
};
