export default {
  OVERVIEW: 'Overview',
  MILESTONES: 'Milestones',
  MEMBERS: 'Members',
  CALENDAR: 'Calendar',
  WAREHOUSE: 'Warehouse',
  DOCUMENTS: 'Documents',
  SETTING: 'Setting',
};
