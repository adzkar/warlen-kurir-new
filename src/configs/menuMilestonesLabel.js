export default {
  TASK: 'Task',
  TASK_REPORT: 'Task Report',
  BUDGET_REPORT: 'Budget Report',
  SETTING: 'Setting',
};
