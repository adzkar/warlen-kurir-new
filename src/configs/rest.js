export default {
  // auth
  LOGIN: '/auth/login',
  REGISTER: '/auth/register',
  ME: '/me',
  PRODUCTS: '/products',
  PRODUCTS_ID: (productId) => {
    return `/products/${productId}`;
  },
  ORDERS: '/courier/orders',
  ORDERS_ID: (orderId) => {
    return `/courier/orders/${orderId}`;
  },
};
