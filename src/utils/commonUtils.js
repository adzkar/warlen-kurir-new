export const timeoutFunc = (timeout, action, delay) => {
  clearTimeout(timeout);
  return setTimeout(() => {
    action();
  }, delay);
};

export const validateEmail = (email) => {
  // eslint-disable-next-line max-len
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const getNickName = (name) => {
  const x = name?.match(/\b\w/g) || [];
  return ((x.shift() || '') + (x.pop() || '')).toUpperCase();
};

export const isObjectEmpty = (value) => {
  return (
    Object.prototype.toString.call(value) === '[object Object]' &&
    JSON.stringify(value) === '{}'
  );
};

export const filterEmptyObject = (value) => {
  const data = {};
  Object.keys(value).forEach((key) => {
    if (value[key]) {
      data[key] = value[key];
    }
  });
  return data;
};
