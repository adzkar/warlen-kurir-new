const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': '#E0003C',
              '@link-color': '#358873',
              '@btn-default-color': '#E0003C',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
// antd documentation https://ant.design/docs/react/customize-theme
// antd documentation complete less
// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
